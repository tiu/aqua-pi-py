Python 3 comes with a web server.

```python
python3 -m http.server 8000 --bind 127.0.0.1
```

Now open your web browser and visit `127.0.0.1:8000`. You will see the current directory.

Create `index.html` and `sensor_data.csv` files. Now refresh your web browser.

Voila!

To run code, we can use the built-in cgi handling.

Create `cgi-bin/sensors.py` that uses our sensor-reading code and prints a response.

```
chmod +x cgi-bin/sensors.py
python3 -m http.server 8000 --cgi
```

We can stop here.

# Your First Server from Scratch

So, you want to build a web server? The Python Standard Library includes [http.server(https://docs.python.org/3.5/library/http.server.html) for implementing HTTP servers.

Lets start small...

```python
python3 ./main1.py
```

Then take another step...

```python
python3 ./main2.py
```

Then call it a day. You shouldn't build your own web server for production.

# Building a "real" web application

Building web applications requires a course all its own.

There are a lot of great packages available.

* [Flask](http://flask.pocoo.org/) - a Python microframework for web development
* [Django](https://www.djangoproject.com/) - a high-level Python framework for web development
* [jinja2](http://jinja.pocoo.org/) - a full-featured template engine for Python

Try a simple Flask app...

```python
FLASK_APP=flask_app.py flask run --host=0.0.0.0
```
