# Introducing Python
#
# Resources:
# * https://projects.raspberrypi.org/en/projects/about-me
# * https://projects.raspberrypi.org/en/projects?software[]=python

# Introduce Python 3 IDLE
# Raspbian comes with Python 3.5.3

print('Hello world!')
print("You're the best!") # interchangeable quoting, escaping
print('Here\'s my cow:')
print('''
    ^__^
    (oo)\_______
    (__)\       )\/\
        ||----w |
        ||     ||
''')

# Useful built-in Functions

help()
help('print')
dir()

# Variables

msg = 'Hi'
print(msg)

# Data Types

print(1) # Integer
print(3.14) # Float
print('hi') # String

# Variable naming: lowercase, underscore_between_words, don't start with numbers

# Data Structures - Advanced
# https://docs.python.org/3.5/tutorial/datastructures.html

list = [1, 2, 3] # list is a mutable collection of values
list[0]
del list[2]

tuple = (1, 2, 3) # tuple is an immutable collection of values
tuple[0]
del tuple[2]

# set is an unordered collection with no duplicate elements
set = {1, 2, 3, 3}
print(set) # notice duplicates have been removed
1 in set # => True

# dictionary is an "associative array" or "hashmap" indexed by keys. Keys can be any immutable type.
dict = {'one': 1, 'two': 2}
dict['one']
'one' in dict # => True

# Control Flow
# https://docs.python.org/3.5/tutorial/controlflow.html

# if statement
# while statement
# for statement

# Use the Python Standard Library (https://docs.python.org/3.5/library/index.html)
# * https://docs.python.org/3.5/library/datetime.html
# * https://docs.python.org/3.5/library/string.html
# * https://docs.python.org/3/library/csv.html

import datetime
datetime.datetime.now()
#=> datetime.datetime(2018, 11, 2, 13, 25, 23, 811860)

from datetime import datetime
datetime.now()
#=> datetime.datetime(2018, 11, 2, 13, 25, 23, 811860)

# String Formatting
now = datetime.now().strftime('%H')
rank = 1
x = 1.234567890
print('%s o\'clock' % now)
print('%d' % x)
print('x is %.2f' % x)
print('x is {:.2f}'.format(x))

# Define Functions
#
# * def
# * function name
# * (parameters)
# * : + indentation (4 spaces)
# * optional documentation string (aka docstring)
# * body
# * return statement

def foo():
    print('foo')

def bar(baz = 3): # parameters, default value
    print('baz is %d' % baz)
    return baz # return value

# Advanced Topics

# Add docstrings to your functions so they work with help()
# See [PEP 257](https://www.python.org/dev/peps/pep-0257/)

def cool_stuff(data):
    """Does cool stuff to your data."""

# Make it an executable unix script by:
# 1) adding the shebang to the first line
#!/usr/bin/env python3
# 2) making the script executable
# chmod +x file.py
# 3) have a piece that only executes when running as a script
if __name__ == '__main__':
    print('main()')

# Break into files

# main.py
# import data_muncher
# data = data_muncher.read()
# print(data)

# data_muncher.py
# def read():
#     return [1, 2, 3]
