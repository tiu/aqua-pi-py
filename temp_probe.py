# Use Immersible Temperature Probe
#
# Connect:
# - blue to 3.3V VCC
# - brown to GND,
# - yellow to GPIO 4

from w1thermsensor import W1ThermSensor
from datetime import datetime

sensor = W1ThermSensor()

while True:
    timestamp = datetime.now().strftime('%H:%M:%S')
    t = sensor.get_temperature()
    print('{} {:.2f}C, {:.2f}F'.format(timestamp, t, t * 9/5 + 32))
    sleep(1)