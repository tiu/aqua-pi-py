# First Web Server

from http.server import HTTPServer, BaseHTTPRequestHandler

PORT = 8000

class MyFirstRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        # Write response headers
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        # Write response body
        content = bytes('Hello World', 'UTF-8')
        self.wfile.write(content)

        # Teaser: comment out the last two lines and read response body from a file instead
        # with open('./index.html') as file:
        #     content = file.read().encode()
        #     self.wfile.write(content)

try:
    httpd = HTTPServer(('', PORT), MyFirstRequestHandler)
    httpd.serve_forever()
except KeyboardInterrupt:
    print('shutting down the web server')
    httpd.server_close()
