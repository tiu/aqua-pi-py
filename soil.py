# Use AutomationHAT with Soil sensor
#
# * Add the AutomationHAT
# * Connect the Capacitive Soil Moisture Sensor
#   red (VCC to 5V)
#   blue (data to ADC 1)
#   black (GND to GND)

import automationhat
from time import sleep
from datetime import datetime

CONFIG = {
    'air': 2.4,
    'dry': 1, # dry sand
    'saturated': 1, # saturated sand  
    'water': 1.4
}

while True:
    value = automationhat.analog.one.read()
    timestamp = datetime.now().strftime('%H:%M:%S')
    print(value, 'at', timestamp)
    sleep(1)