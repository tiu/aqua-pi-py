#!/usr/bin/env python3

# SenseHAT
#
# Resources:
# * https://projects.raspberrypi.org/en/projects/getting-started-with-the-sense-hat
# * https://projects.raspberrypi.org/en/projects/sense-hat-data-logger
# * https://pythonhosted.org/sense-hat/

from sense_hat import SenseHat
from datetime import datetime
import time
import csv

sense = SenseHat()
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S.%f'
COLUMN_NAMES = ('Time', 'Temperature (C)', 'Humidity (% rH)', 'Pressure (mbar)')
INTERVAL = 5 # seconds between measurements

sense.low_light = True
sense.set_rotation(180)

def say(msg = 'Hello World!'):
    sense.show_message(msg)

t_c = sense.get_temperature() # Celsius
t_f = t_c * 9 / 5 + 32 # Fahrenheit

def countdown():
    for i in reversed(range(0,10)):
        sense.show_letter(str(i))
        time.sleep(0.5)

def show_env():
    t = sense.get_temperature() # Celsius (C)
    h = sense.get_humidity() # % relative humidity (% rH)
    p = sense.get_pressure() # Millibars (mbar)
    msg = '{:.1f}C  {:.1f}%rH {:.1f}mbar'.format(t, h, p)
    sense.show_message(msg, text_colour=(100, 100, 100))

def read_env():
    return (
        datetime.now().strftime(DATETIME_FORMAT),
        sense.get_temperature(),
        sense.get_humidity(),
        sense.get_pressure()
    )

def current_readings():
    return {
        'timestamp': datetime.now(),
        'temperature': sense.get_temperature(),
        'humidity': sense.get_humidity(),
        'pressure': sense.get_pressure()
    }

def write_env():
    with open('sensor_data.csv', 'a', newline='') as file:
        writer = csv.writer(file)
        # writer.writerow(COLUMN_NAMES) # TODO: only write for new file
        writer.writerow(read_env())

def log_continuously(arg, delay = INTERVAL):
    with open('sensor_data.csv', 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(COLUMN_NAMES) # TODO: only write for new file
        while True:
            data = read_env()
            writer.writerow(data)

# Cron job
# crontab -e
# 1 * * * * python3 /home/pi/aquapi-py/log_sense_hat.py

if __name__ == '__main__':
    print('Writing sensor readings to CSV file...')
    write_env()
    # countdown()
    # sense.clear()
