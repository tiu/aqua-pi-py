# First Web Server

from http.server import HTTPServer, BaseHTTPRequestHandler
import os

PORT = 8000

class MyFirstRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/':
            with open('./index.html') as file:
                content = file.read().encode()
                content_type = 'text/html'
        elif self.path == '/sensor_data.csv':
            with open('./sensor_data.csv') as file:
                content = file.read().encode()
                content_type = 'text/csv'
        else:
            content = bytes('Hello World', 'UTF-8')
            content_type = 'text/html'

        # Write response headers
        self.send_response(200)
        self.send_header('Content-type', content_type)
        self.end_headers()

        # Write response body
        self.wfile.write(content)

try:
    httpd = HTTPServer(('', PORT), MyFirstRequestHandler)
    httpd.serve_forever()
except KeyboardInterrupt:
    print('shutting down the web server')
    httpd.server_close()
