# Raspberry Pi Academy Aquaponics

Using Python to do things

Get this with `git clone https://bitbucket.org/tiu/aqua-pi-py.git`

## Intro to Python

[Intro](./intro.py)

###### Resources

* [The Python Tutorial](https://docs.python.org/3.5/tutorial)
  > Python is an easy to learn, powerful programming language.
* [The Python Language Reference](https://docs.python.org/3.5/reference/)
  > This reference manual describes the syntax and “core semantics” of the language. It is terse, but attempts to be exact and complete.
* [The Python Standard Library](https://docs.python.org/3.5/library/)
  > This library reference manual describes the standard library that is distributed with Python. It also describes some of the optional components that are commonly included in Python distributions.

  * [string — Common string operations](https://docs.python.org/3.5/library/string.html)
  * [datetime — Basic date and time types](https://docs.python.org/3.5/library/datetime.html)
  * [csv — CSV File Reading and Writing](https://docs.python.org/3.5/library/csv.html)

These resources focus on learning Python, using just libraries that ship with Python, not some of the many, excellent libraries that are available. For why, see [Learning Python without Library Overload](https://chrisconlan.com/learning-python-without-library-overload/) and a [discussion of it on Hacker News](https://news.ycombinator.com/item?id=15164948).

###### Raspberry Pi Projects
* https://projects.raspberrypi.org/en/projects/raspberry-pi-getting-started
* https://projects.raspberrypi.org/en/projects/about-me

## Log Sensor Data

Uses sensorHAT

[Log Sensor Data](./log_sense_hat.py)

###### Resources

* https://pythonhosted.org/sense-hat/

###### Raspberry Pi Projects

* https://projects.raspberrypi.org/en/projects/about-me
* https://projects.raspberrypi.org/en/projects/getting-started-with-the-sense-hat

## Build a Web Server to Retrieve Data

[Server](./server/SERVER.md)

###### Resources

* [An overview of HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)
* [HTTP response status codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
* [Incomplete list of MIME types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types)


## Going Further

* https://projects.raspberrypi.org/en/projects/getting-started-with-the-twitter-api

Sensor Documentation
* SenseHAT - https://pythonhosted.org/sense-hat/
* Picademy Parts Kit Immersible Temperature Probe
  - http://ksuweb.kennesaw.edu/faculty/rbrow211/raspberrypi/temp_probe/
  - https://cdn-shop.adafruit.com/datasheets/DS18B20.pdf
  - 3.3V VCC (blue), GND (brown), OUT (yellow)
* Capacitive Soil Moisture Sensor
  - 3.3-5V VCC (red), GND (black), OUT

Other
* [4 Band Resistor Color Code Calculator](https://www.digikey.com/en/resources/conversion-calculators/conversion-calculator-resistor-color-code-4-band)

###

* [Become a git guru](https://www.atlassian.com/git/tutorials)
