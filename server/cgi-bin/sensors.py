#!/usr/bin/env python3

from sense_hat import SenseHat
from datetime import datetime

sense = SenseHat()
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S.%f'

def current_readings():
    '''Return dict of current sensor readings.'''
    return {
        'timestamp': datetime.now().strftime(DATETIME_FORMAT),
        'temperature': sense.get_temperature(),
        'humidity': sense.get_humidity(),
        'pressure': sense.get_pressure()
    }

if __name__ == '__main__':
    readings = current_readings()
    template = '''
        <!doctype html>
        <title>Aquaponics Pi</title>
        <h1>Current Sensor Readings</h1>

        Time: {0}                    <br />
        Temperature: {1:.2f} &deg;C
                    ({2:.2f} &deg;F) <br />
        Humidity: {3:.2f} %rH        <br />
        Pressure: {4:.2f} mbar       <br />
    '''

    print('Content-Type: text/html\n')
    print(template.format(
        readings['timestamp'],
        readings['temperature'],
        readings['temperature'] * 9 / 5 + 32,
        readings['humidity'],
        readings['pressure']))
